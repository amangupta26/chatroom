//client side script to handle socket IO
function userconnect() {
  
  var socket = io.connect('http://localhost:3000');
  var sessionId = '';

  //action done when client receives "connect" event from server
  socket.on('connect', function () {
    sessionId = socket.io.engine.id;
    socket.emit('userJoined',{SessionId : sessionId});   //emit an event after joining the chatroom 
  });

  //event when someone in chatroom sends a message
  socket.on('receiveMessage',function(data){
    var name = data.name;
    var message = data.message;
    $('#chatbox').prepend('<p>' + name +'</p><br />' + message );  
  });

  //add newly entered user in the list
  socket.on("addUserList",function(data){
    //to do
  });

  //delete the user that has logged out
  socket.on("deleteUserList",function(data){
    //to do
  });

  function sendMessage(){
    var message = $('#messagetext').val();
    $.ajax({
      url : '/chat',
      type : 'POST',
      contentType: 'application/json',
      dataType: 'json',
      data: JSON.stringify({message: message})
    });
  }

  //send message when send button is clicked
  $('#messagebutton').on('click',sendMessage);


}

$(document).on('ready', userconnect);