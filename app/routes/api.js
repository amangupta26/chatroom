var User = require('../models/user');
module.exports = function(app,express){
	var api = express.Router();

	//API to handle signup
	api.post('/signup',function(req,res){
		var user = new User({
			user : req.body.user
		});

		user.save(function(err){
			if(err)
				res.send(err);
			else
				res.json({success: true,message : "User created succesfully"});
		});
	});

	//API to handle login
	api.post('/login',function(req,res){
		var user_logged_in = req.body.user;
		req.session.user = user_logged_in;
		User.findOne({user : user_logged_in},function(err,user){
			if(err)
				res.json({message : "User not the part of the chatroom"});
			else{
				console.log(user);
				user.isActive = true;
				user.save(function(err){
					if(err)
						res.json({success : false,message : "Try Again!!!"});
					else{
						//res.json({success : true,message : "Succesful Login"});
						res.redirect('chat');
					}
				});
				
			}
		});
	});

	//API to handle logout
	api.get('/logout',function(req,res){
		req.session.user = null;
		return res.json({message : "Logged Out"});
	});


	/////////////////Middleware////////////////////
	//Middleware to make sure user is logged in
	api.use(function(req,res,next){
		if(req.session.user){						//user should have a session with server before going through next routes
			req.body.user = req.session.user;
			res.redirect('chat.jade');
			next();							
		}
		else
			res.status(403).send({message : "invalid user"});
	});

	//API to handle mesage posted by user
	api.post('/message',function(req,res){
		var user = req.session.user;
		var message = req.body.message;
		res.json({user : user, message : message});		
	});

	//API to get all users registered with the site
	api.get('/users',function(req,res){
		User.find({},{user : 1, _id : 0},function(err,users){
			if(err)
				res.send(err);
			else
				res.json({users : users });
		});
	});

	//API to get list of users which are currently active 
	api.get('/activeUsers',function(req,res){
		User.find({isActive : true},{user : 1, _id : 0},function(err,users){
			if(err)
				res.send(err);
			else	
				res.json({users : users });
		});
	});



	return api;
};