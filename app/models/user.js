var mongoose = require('mongoose');	


//schema for user signed up on the website
var UserSchema = new mongoose.Schema({
		user : String,
		isActive : {type :Boolean, default : false}
});

module.exports = mongoose.model('User',UserSchema);