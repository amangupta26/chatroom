var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var _ = require('underscore');
var mongoose = require('mongoose');
var config = require('./config');
var User = require('./app/models/user');

var app = express();

//spath for the views, views can be referred relative to this path
app.set('views', __dirname + '/public/app/views');

//templating engine used by express, here - "jade"
app.set('view engine', 'jade');

//create a sesiion between logged in client and the server
app.use(session({secret: 'secret'}));

//parses incoming post requests from client
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//logs request/response status etc. between client and the server
app.use(morgan('dev'));


//registers route with the server
var route = require('./app/routes/api')(app,express);
app.use('/api',route);

//folder to look for static files like css,html etc
app.use(express.static(__dirname + "/public"));

//handles GET request to the server on the URI "/"
app.get('/',function (req, res) {
  	res.render('index', { title: 'Hey'});
});

//handles POST request to the server on the URI "/chat"
app.post('/chat',function(req,res){
	var message = req.body.message;
	var name = req.body.name;
	io.sockets.emit("receiveMessage", {message: message, name: name});	//broadcasts message to all users when a user sends a message
});



//connects to mongoDB database
mongoose.connect(config.database,function(err){
	if(err)
		console.log("Could not conect to the database");
	else{
		console.log("Database connected");

		//server listens on reqquest on port 3000 and default IP i.e. localhost
		var server = app.listen(3000,function(){
			console.log("Server Started");
		});

		//listen and send socket events
		var io = require('socket.io').listen(server);

		//listen for event "connection"
		io.on('connection', function(socket){
		  socket.on("userJoined",function(data){
		  	io.socket.emit("addUserList",{user : user});
		  });

		  socket.on("disconnect",function(){
		  	io.socket.emit("deleteUserList",{user : user});
		  });
		});
	}
});